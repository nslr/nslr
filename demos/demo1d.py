import nslr
import numpy as np
import matplotlib.pyplot as plt
import scipy.signal

# Define noise and split rate of the signal
noise_std = 0.5
split_rate = 1/3.0

# Create a test signal
ts = np.arange(0, 10, 1/60.0)

# Create the ground-thruth
xs_truth = scipy.signal.sawtooth(ts/split_rate)
# Add noise to simulate a noisy signal
xs = xs_truth + np.random.randn(len(xs_truth))*noise_std

# Run tne NSLR
segments = nslr.nslr1d(ts, xs, noise_std, split_rate)

# Plot the results
plt.plot(ts, xs, 'k.', label="Measurments")
plt.plot(ts, xs_truth, 'g-', label='Ground thruth')
plt.plot(segments.t, segments.x, 'r.-', label="NSLR result")
plt.xlabel("Time")
plt.ylabel("Signal value")
plt.legend()
plt.show()

